use std::{env, io};

fn print_mind(text: String, max_line_len: usize) {
    let args: Vec<String> = env::args().collect();
    let text = text.replace("\\n", "\n");

    let splitted_mind = whalesay::split_mind(&text, max_line_len, args.contains(&"-n".into()));

    whalesay::to_mind(&splitted_mind).iter().for_each(|l|println!("{l}"));
    whalesay::print_whale();
}

fn get_text_from_args(_args: Vec<String>) -> Option<Vec<String>> {
    let result = _args
        .into_iter()
        .skip(1)
        .filter(|arg| !arg.starts_with('-'))
        .collect::<Vec<String>>();

    match result.len() {
        0 => None,
        _ => Some(result),
    }
}

static HELP: &str = "whalesay [-n, --help] \"text\"
-n flag used for saving line breaks.    
\nalso can display pipe text. e.g. `ls / | whalesay`";

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.contains(&"--help".to_string()) {
        println!("{}", HELP);
    } else {
        let text = match get_text_from_args(args) {
            Some(txt) => txt.iter().fold(String::new(), |a, b| a + b + " "),
            None => match io::read_to_string(io::stdin()) {
                Ok(text) => text,
                Err(_) => {
                    eprintln!("No text provided in stdio.");
                    std::process::exit(1);
                }
            },
        };

        print_mind(text.trim().to_string(), 70);
    }
}
