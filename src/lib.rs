const WHALE: &str = "      \\         .                  \0       \\       \":\"               \0        \\    ___:____     |\"\\/\"|\0         \\ ,'        `.    \\  /   \0           |  O        \\___/  |    \0         ~^~^~^~^~^~^~^~^~^~^~^~^~";

fn get_len(_str: &str) -> usize {
    _str.chars().count() as usize
}

fn get_max_len(strs: &Vec<String>) -> usize {
    strs.iter().map(|str| get_len(&str)).max().unwrap_or(0)
}

fn split(text: &str, max_len: usize) -> Vec<String> {
    let mut result: Vec<String> = Vec::new();

    let mut buf: String = String::new();
    text.split_whitespace().for_each(|word| {
        if get_len(word) > max_len {
            let mut remain = word;
            while !remain.is_empty() {
                if get_len(remain) < max_len {
                    buf = remain.to_string();
                    break;
                } else {
                    let (p, r) = remain.split_at(max_len);
                    result.push(p.to_string());
                    remain = r;
                }
            }
        } else if buf.is_empty() {
            buf.push_str(word);
        } else if get_len(&buf) + get_len(word) + 1 <= max_len {
            buf.push_str(format!(" {}", word).as_str());
        } else {
            result.push(buf.clone());
            buf = word.to_string();
        }
    });
    if !buf.is_empty() {
        result.push(buf);
    }

    result
}

pub fn split_mind(text: &str, max_line_len: usize, save_newlines: bool) -> Vec<String> {
    let mut result = Vec::<String>::new();

    if text.contains("\n") && save_newlines {
        text.split("\n").for_each(|t| {
            if !t.is_empty() {
                split(t, max_line_len).iter().for_each(|line| {
                    result.push(line.to_string());
                });
            }
        });
    } else {
        result = split(&text, max_line_len);
    }

    result
}

fn get_surrounders(lines_count: usize, line_n: usize) -> (char, char) {
    if lines_count == 1 {
        ('(', ')')
    } else {
        match line_n {
            0 => ('/', '\\'),                              // first line
            _ if line_n == lines_count - 1 => ('\\', '/'), // last line
            _ => ('|', '|'),                               // any other line
        }
    }
}

pub fn to_mind(mind: &Vec<String>) -> Vec<String> {
    let text_max_len = get_max_len(mind);
    let lines_count = mind.len();

    let mut result = mind
        .iter()
        .enumerate()
        .map(|(i, line)| {
            let surrounders = get_surrounders(lines_count, i);
            format!(
                " {} {line}{space} {}",
                surrounders.0,
                surrounders.1,
                space = " ".repeat(text_max_len - get_len(line)),
            )
        })
        .collect::<Vec<String>>();

    result.insert(0, format!("  {}", "_".repeat(text_max_len+2)));
    result.push(format!("  {}", "-".repeat(text_max_len+2)));

    result
}

pub fn print_whale() {
    println!("{}", WHALE.replace('\0', "\n"));
}

pub fn get_whale() -> Vec<String> {
    WHALE.split('\0').map(|s| s.into()).collect::<Vec<String>>()
}
